package PresentationLayer;

import Model.Client;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ClientView extends JFrame {

    JTable table = new JTable();
    private JLabel idC= new JLabel("ID CLIENT:");
    private JLabel nameC= new JLabel("NAME :");
    private JLabel address= new JLabel(" ADDRESS:");
    private JLabel phone = new JLabel("PHONE NB :");
    private JTextField idCT=new JTextField(5);
    private JTextField nameCT= new JTextField(5);
    private JTextField addressT= new JTextField(10);
    private JTextField phoneT= new JTextField(10);
    private  JScrollPane tables=new JScrollPane();


    private JButton addClient=new JButton("ADD CLIENT");
    private JButton editClient=new JButton("EDIT CLIENT");
    private JButton deleteClient=new JButton("DELETE CLIENT");
    private JButton viewClients=new JButton("VIEW CLIENTS");
    public JPanel p1=new JPanel();
    public JPanel p2=new JPanel();
    public ClientView() {
        p1.add(idC);
        p1.add(idCT);
        p1.add(nameC);
        p1.add(nameCT);
        p1.add(address);
        p1.add(addressT);
        p1.add(phone);
        p1.add(phoneT);

       // p1.setAlignmentX(BOTTOM_ALIGNMENT);
        p1.add(tables);

        p2.add(addClient);
        p2.add(editClient);
        p2.add(deleteClient);
        p2.add(viewClients);

        p1.add(p2);

        this.add(p1);
        this.setTitle("Table Clienti");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)  ;
        //setBounds(160, 500, 600, 600);
        this.setBounds(500,500,700,600);
        //this.pack();
        //this.setVisible(true);
    }
    void addNewClientListener(ActionListener p1) {
        addClient.addActionListener(p1);
    }

    void addEditClientListener(ActionListener p2) {
        editClient.addActionListener(p2);
    }

    void addDeletClientListener(ActionListener p3) {
        deleteClient.addActionListener(p3);
    }

    void addViewClientsListener(ActionListener p4) {
        viewClients.addActionListener(p4);
    }
    public JTable getTable() {
        return table;


    }

    public void setTable(JTable table) {

        this.table = table;
        tables.setViewportView(table);

    }

    public String getIdCT() {
        return idCT.getText();
    }

    public void setIdCT(JTextField idCT) {
        this.idCT = idCT;
    }

    public String getNameCT() {
        return nameCT.getText();
    }

    public void setNameCT(JTextField nameCT) {
        this.nameCT = nameCT;
    }

    public String getAddressT() {
        return addressT.getText();
    }

    public void setAddressT(JTextField addressT) {
        this.addressT = addressT;
    }

    public String getPhoneT() {
        return phoneT.getText();
    }

    public void setPhoneT(JTextField phoneT) {
        this.phoneT = phoneT;
    }

    public JButton getAddClient() {
        return addClient;
    }

    public void setAddClient(JButton addClient) {
        this.addClient = addClient;
    }

    public JButton getEditClient() {
        return editClient;
    }

    public void setEditClient(JButton editClient) {
        this.editClient = editClient;
    }

    public JButton getDeleteClient() {
        return deleteClient;
    }

    public void setDeleteClient(JButton deleteClient) {
        this.deleteClient = deleteClient;
    }

    public JButton getViewClients() {
        return viewClients;
    }

    public void setViewClients(JButton viewClients) {
        this.viewClients = viewClients;
    }
}
