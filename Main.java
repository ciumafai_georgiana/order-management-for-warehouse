import BusinessLayer.ClientBLL;
import BusinessLayer.OrderBLL;
import BusinessLayer.ProductBLL;
import DataAcces.ClientDAO;
import DataAcces.OrderDAO;
import DataAcces.ProductDAO;
import Model.Client;
import Model.OrderClient;
import Model.Product;
import PresentationLayer.*;

public class Main {
    public static ClientView clientView;
    public static View view;
    private static Controller controller;
    private static ProductView productView;
    private static OrderView orderView;
    //public Controller c;
    public static void main(String[] args) {
       ClientBLL clientBLL=new ClientBLL();
        //clientBLL.findAll();
        //ClientDAO c=new ClientDAO();
        //Client c2=c.findClientById(1);
        //System.out.println(c2);
        //System.out.println(clientBLL.findById(1));
        //System.out.println(clientBLL.findAll());
        //Client c3=new Client(4,"da","ds","sd");
        //clientBLL.persist(c3);
        //System.out.println(clientBLL.findById(4));
        //c3.setAddress("ce3nd");
        //clientBLL.update(c3);
        //System.out.println(clientBLL.findById(4));
        //clientBLL.delete(4);
        //System.out.println(clientBLL.findById(4));


        OrderBLL orderBLL=new OrderBLL();
        //OrderDAO orderDAO=new OrderDAO();
        //OrderClient o=orderDAO.findOrderById(1);
        //System.out.println(o);
        //System.out.println(orderDAO.findOrderById(1));
        //orderBLL.delete(1);
        //orderBLL.delete(2);
        OrderClient o1=new OrderClient(1,1,1,4,4000);
        OrderClient o2=new OrderClient(2,2,2,4,4400);
        //orderBLL.persist(o1);
       // orderBLL.persist(o2);

        ProductBLL productBLL=new ProductBLL();
        //ProductDAO productDAO=new ProductDAO();
        //Product p=productDAO.findProductById(1);
        //System.out.println(p);
        //System.out.println(productBLL.findAll());
        Product p1=new Product(1,"Iphone X",300,1000);
        Product p2=new Product(2,"Samsung S10",300,1100);
        //productBLL.persist(p1);
        //productBLL.persist(p2);
        //p2.setNameProduct("lala");
        //productBLL.update(p2);
        //System.out.println(productBLL.findById(6));
        //productBLL.delete(6);
        //System.out.println(productBLL.findById(6));

         //productBLL.delete(1);
         //productBLL.delete(2);
         //productBLL.delete(4);
        //View v=new View();
        //ClientView c=new ClientView();

       // g=new Gui();
        view=new View();
        clientView=new ClientView();
        productView=new ProductView();
        orderView=new OrderView();
        controller=new Controller(view,clientBLL,clientView,productBLL,productView,orderBLL,orderView);
    }
}
