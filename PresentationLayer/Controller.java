package PresentationLayer;

import BusinessLayer.ClientBLL;
import BusinessLayer.OrderBLL;
import BusinessLayer.ProductBLL;
import Model.Client;
import Model.OrderClient;
import Model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    View view;
    ClientView clientView;
    ProductView productView;
    OrderView orderView;
    ClientBLL clientBLL=new ClientBLL();
    ProductBLL productBLL=new ProductBLL();
    OrderBLL orderBLL=new OrderBLL();
    public Controller(View view, ClientBLL clientBLL, ClientView clientView,ProductBLL productBLL,ProductView productView,OrderBLL orderBLL,OrderView orderView){
        this.view=view;
        this.clientBLL=clientBLL;
        this.clientView=clientView;
        this.productBLL=productBLL;
        this.productView=productView;
        this.orderBLL=orderBLL;
        this.orderView=orderView;
        clientView.addViewClientsListener(new ViewAllClients());
        clientView.addNewClientListener(new AddClient());
        clientView.addEditClientListener(new UpdateClient());
        clientView.addDeletClientListener(new DeleteClient());
        productView.addViewProductsListener(new ViewAllProducts());
        productView.addNewProductListener(new AddProduct());
        productView.addEditProductListener(new UpdateProduct());
        productView.addDeleteProductListener(new DeleteProduct());
        orderView.addNewCreateOListener(new OrderPr());
        view.ClientListener(new ClientsA());
        view.ProductListener(new ProductsA());
        view.CreateOrderListener(new OrderA());

    }
    private class OrderPr implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            try {
                Product p=productBLL.findById(Integer.parseInt(orderView.getIdPT()));
                int price=p.getPrice();
                if(p.getQuantityInStock()>Integer.parseInt(orderView.getQuantityT())) {
                    OrderClient o1 = new OrderClient(14, Integer.parseInt(orderView.getIdCT()), Integer.parseInt(orderView.getIdPT()), Integer.parseInt(orderView.getQuantityT()), Integer.parseInt(orderView.getQuantityT()) * price);
                    orderBLL.persist(o1);
                    orderView.setTable(orderBLL.getTable());
                    p.setQuantityInStock(p.getQuantityInStock()-Integer.parseInt(orderView.getQuantityT()));
                    productBLL.update(p);
                }
                else
                {
                    JOptionPane.showMessageDialog(orderView, "Stock is low");
                }
                orderView.setVisible(true);
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    private class OrderA implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            try {
                orderView.setVisible(true);
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    private class ViewAllClients implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            try {
                clientView.setTable(clientBLL.getTable());
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    private class AddClient implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Client c= new Client(Integer.parseInt(clientView.getIdCT()),clientView.getNameCT(),clientView.getAddressT(),clientView.getPhoneT());
                clientBLL.persist(c);
                clientView.setTable(clientBLL.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    private class DeleteClient implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Client c= new Client(Integer.parseInt(clientView.getIdCT()),clientView.getNameCT(),clientView.getAddressT(),clientView.getPhoneT());
                clientBLL.delete(c.getIdC());
                clientView.setTable(clientBLL.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    private class UpdateClient implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Client c= new Client(Integer.parseInt(clientView.getIdCT()),clientView.getNameCT(),clientView.getAddressT(),clientView.getPhoneT());
                clientBLL.update(c);
                clientView.setTable(clientBLL.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    private class ClientsA implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            try {
                clientView.setVisible(true);
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }

    private class ProductsA implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            try {
                productView.setVisible(true);
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    private class ViewAllProducts implements ActionListener{
        public void actionPerformed(ActionEvent e) {
            try {
                productView.setTable(productBLL.getTable());
            }
            catch(Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
    private class AddProduct implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Product p= new Product(Integer.parseInt(productView.getIdPT()),productView.getNamePT(),Integer.parseInt(productView.getQuantityT()),Integer.parseInt(productView.getPriceT()));
                productBLL.persist(p);
                productView.setTable(productBLL.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    private class DeleteProduct implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Product p= new Product(Integer.parseInt(productView.getIdPT()),productView.getNamePT(),Integer.parseInt(productView.getQuantityT()),Integer.parseInt(productView.getPriceT()));
                productBLL.delete(p.getIdP());
                productView.setTable(productBLL.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
    private class UpdateProduct implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                Product p= new Product(Integer.parseInt(productView.getIdPT()),productView.getNamePT(),Integer.parseInt(productView.getQuantityT()),Integer.parseInt(productView.getPriceT()));
                productBLL.update(p);
                productView.setTable(productBLL.getTable());
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }
        }
    }
}
