package DataAcces;

import Model.Product;

public class ProductDAO extends AbstractDAO<Product> {

    public Product findProductById(int id){
        return super.findById(id);
    }
}
