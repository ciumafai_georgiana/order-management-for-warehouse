package DataAcces;
import Model.*;
import Connection.ConnectionFactory;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Clasa AbstractDao contine metodele CRUD pentru toate clasele
 * @author      Ciumafai Georgiana
 */
public class AbstractDAO<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }
    /**
     * Creeaza o interogare de tip SELECT din un tabel
     * @param field nume field
     * @return Returneaza interogarea sub forma de String
     */
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM "+type.getSimpleName());
        sb.append(" WHERE " + field + "=?");
        return sb.toString();
    }
    /**
     * Creeaza o interogare de tip SELECT * din un tabel
     * @return Returneaza interogarea sub forma de String
     */
    private String createFindAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM "+type.getSimpleName());
        return sb.toString();
    }
    /**
     * Creeaza o interogare de tip Delete din un tabel
     * @param field nume field
     * @return Returneaza interogarea sub forma de String
     */
    private String createDeleteQuery(String field){
        StringBuilder sb= new StringBuilder();
        sb.append("DELETE FROM "+type.getSimpleName()+" ");
        sb.append("WHERE "+field+" = ?");
        System.out.println(sb.toString());
        return sb.toString();

    }
    /**
     * Creeaza o interogare de tip DeleteAll din un tabel
     * @return Returneaza interogarea sub forma de String
     */
    private String createDeleteAllQuery(){
        StringBuilder sb= new StringBuilder();
        sb.append("DELETE FROM "+type.getSimpleName()+" ");
        System.out.println(sb.toString());
        return sb.toString();

    }
    /**
     * Creeaza o interogare de tip Insert din un tabel
     * @return Returneaza interogarea sub forma de String
     */
    private String createInsertQuery(){
        StringBuilder sb=new StringBuilder();
        String fields = "(";
        String values = " VALUES(";
        Field[] f=type.getDeclaredFields();
        for(int i=0;i<type.getDeclaredFields().length-1;i++){
                values = values + "?,";
                fields = fields + f[i].getName() + ",";
        }
        fields = fields + f[type.getDeclaredFields().length-1].getName();
        values = values + "?";

        sb.append("INSERT INTO "+type.getSimpleName()+" "+fields+")");
        sb.append(values+")");
        return sb.toString();
    }
    /**
     * Creeaza o interogare de tip Update din un tabel
     * @param field nume field
     * @return Returneaza interogarea sub forma de String
     */
    private String createUpdateQuery(String field){
        StringBuilder sb=new StringBuilder();
        String fields = "";
        Field[] f=type.getDeclaredFields();
        for(int i=1;i<type.getDeclaredFields().length-1;i++){
                fields = fields + f[i].getName() + "=?,";
        }
        fields = fields + f[type.getDeclaredFields().length-1].getName()+"=?";
        sb.append("UPDATE "+type.getSimpleName()+" SET "+fields+"");
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }


    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Field[] f=type.getDeclaredFields();
        String query = createSelectQuery(f[0].getName());
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObject(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
            while (e != null) {
                String errorMessage = e.getMessage();
                System.err.println("sql error message:" + errorMessage);

                // This vendor-independent string contains a code.
                String sqlState = e.getSQLState();
                System.err.println("sql state:" + sqlState);

                int errorCode = e.getErrorCode();
                System.err.println("error code:" + errorCode);
                // String driverName = conn.getMetaData().getDriverName();
                // System.err.println("driver name:"+driverName);
                // processDetailError(drivername, errorCode);
                e = e.getNextException();
            }
            //JOptionPane.showMessageDialog(null, e);
        } catch(IndexOutOfBoundsException ex) {
            return null;
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    public List<T> findAll( ){
        Connection connection=null;
        PreparedStatement statement=null;
        ResultSet resultSet=null;
        String query=createFindAllQuery();
        try{
            connection= ConnectionFactory.getConnection();
            statement=connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            //statement.setInt(1,id);
            return createObject(resultSet);

        }
        catch (SQLException e){
            LOGGER.log(Level.WARNING,type.getName()+"DAO:findAll" +e.getMessage());
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;

    }
    public T insert(T tObj ){
        Connection connection=null;
        PreparedStatement statement=null;
        //ResultSet resultSet=null;
        String query=createInsertQuery();
        Field[] f=type.getDeclaredFields();
        int j;
        try{
            connection= ConnectionFactory.getConnection();
            statement=connection.prepareStatement(query);

            for(int i=0;i<type.getDeclaredFields().length;i++) {
                f[i].setAccessible(true);
                j=i+1;
                statement.setObject(j, f[i].get(tObj));
            }
             statement.executeUpdate();
        }
        catch (SQLException e){
            LOGGER.log(Level.WARNING,type.getName()+"DAO:insert" +e.getMessage());
            while (e != null) {
                String errorMessage = e.getMessage();
                System.err.println("sql error message:" + errorMessage);

                // This vendor-independent string contains a code.
                String sqlState = e.getSQLState();
                System.err.println("sql state:" + sqlState);

                int errorCode = e.getErrorCode();
                System.err.println("error code:" + errorCode);
                // String driverName = conn.getMetaData().getDriverName();
                // System.err.println("driver name:"+driverName);
                // processDetailError(drivername, errorCode);
                e = e.getNextException();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            //ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;

    }
    public T update(T tObj ){
        Connection connection=null;
        PreparedStatement statement=null;
        //ResultSet resultSet=null;
        Field[] f=type.getDeclaredFields();
        String query=createUpdateQuery(f[0].getName());

        try{
            connection= ConnectionFactory.getConnection();
            statement=connection.prepareStatement(query);

            for(int i=1;i<type.getDeclaredFields().length;i++) {
                f[i].setAccessible(true);
                statement.setObject(i, f[i].get(tObj));
            }
            //statement.setInt(1,id);
            f[0].setAccessible(true);
            statement.setObject(type.getDeclaredFields().length,f[0].get(tObj));
           // statement.executeUpdate();
            statement.executeUpdate();


        }
        catch (SQLException e){
            LOGGER.log(Level.WARNING,type.getName()+"DAO:update" +e.getMessage());

            while (e != null) {
                String errorMessage = e.getMessage();
                System.err.println("sql error message:" + errorMessage);

                // This vendor-independent string contains a code.
                String sqlState = e.getSQLState();
                System.err.println("sql state:" + sqlState);

                int errorCode = e.getErrorCode();
                System.err.println("error code:" + errorCode);
                // String driverName = conn.getMetaData().getDriverName();
                // System.err.println("driver name:"+driverName);
                // processDetailError(drivername, errorCode);
                e = e.getNextException();
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            //ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;

    }
    public void deleteById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        Field[] f=type.getDeclaredFields();
        String query = createDeleteQuery(f[0].getName());
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
            while (e != null) {
                String errorMessage = e.getMessage();
                System.err.println("sql error message:" + errorMessage);

                // This vendor-independent string contains a code.
                String sqlState = e.getSQLState();
                System.err.println("sql state:" + sqlState);

                int errorCode = e.getErrorCode();
                System.err.println("error code:" + errorCode);
                // String driverName = conn.getMetaData().getDriverName();
                // System.err.println("driver name:"+driverName);
                // processDetailError(drivername, errorCode);
                e = e.getNextException();
            }
           // JOptionPane.showMessageDialog(null, e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        //return null;
    }
    public void deleteAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createDeleteAllQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            //statement.setInt(1, id);
            statement.executeQuery();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:delete " + e.getMessage());
            while (e != null) {
                String errorMessage = e.getMessage();
                System.err.println("sql error message:" + errorMessage);

                // This vendor-independent string contains a code.
                String sqlState = e.getSQLState();
                System.err.println("sql state:" + sqlState);

                int errorCode = e.getErrorCode();
                System.err.println("error code:" + errorCode);
                // String driverName = conn.getMetaData().getDriverName();
                // System.err.println("driver name:"+driverName);
                // processDetailError(drivername, errorCode);
                e = e.getNextException();
            }
            // JOptionPane.showMessageDialog(null, e);
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        //return null;
    }
    /**
     * Creeaza o lista de elemente de tip T
     * @param resultSet setul de rezultate din urma unei interogari
     * @return Returneaza o lista de elemente de tip T
     */

    private List<T> createObject(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }
    public String[] columns(Object obj){
        Field[] f = type.getDeclaredFields();
        String[] columnName = new String[type.getDeclaredFields().length];
        for (int i = 0; i < type.getDeclaredFields().length; i++) {
            f[i].setAccessible(true);
            columnName[i] = f[i].getName();
        }
        return columnName;
    }
    public JTable createTable(List<T> obj) {
        String[] c=columns(type.getClass());
        Field[] f = type.getDeclaredFields();
        DefaultTableModel model = new DefaultTableModel(c, 0);
        for (T t : obj) {
            Object[] data = new Object[type.getDeclaredFields().length];
            try {
                for (int i = 0; i < type.getDeclaredFields().length; i++) {
                    f[i].setAccessible(true);
                    data[i] = f[i].get(t);
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
            model.addRow(data);
        }
        JTable table = new JTable(model);
        return table;
    }

    //protected abstract Class<?> getEntityClassName();
}
