package PresentationLayer;

import javax.swing.*;
import java.awt.event.ActionListener;

public class OrderView extends JFrame {
    private JLabel idProduct= new JLabel("ID PRODUCT:");
    private JLabel idClient= new JLabel("ID CLIENT:");
    private JLabel quantity= new JLabel("QUANTITY:");
    private JTextField idPT=new JTextField(5);
    private JTextField idCT= new JTextField(5);
    private JTextField quantityT= new JTextField(5);
    private JTable table=new JTable();
    private JScrollPane t2=new JScrollPane();
    private JButton createO=new JButton("Create Order");
    private JPanel p1=new JPanel();
    private JPanel p2=new JPanel();
    public OrderView(){
        p1.add(idProduct);
        p1.add(idPT);
        p1.add(idClient);
        p1.add(idCT);
        p1.add(quantity);
        p1.add(quantityT);

        p1.add(t2);

        // p1.setAlignmentX(BOTTOM_ALIGNMENT);


        p2.add(createO);

        p1.add(p2);

        this.add(p1);
        this.setTitle("Table Order");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)  ;
        //setBounds(160, 500, 600, 600);
        this.setBounds(500,500,700,600);
        //this.pack();
        //this.setVisible(true);
    }
    void addNewCreateOListener(ActionListener p1) {
        createO.addActionListener(p1);
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
        t2.setViewportView(table);
    }
    public String getIdPT() {
        return idPT.getText();
    }

    public void setIdPT(JTextField idPT) {
        this.idPT = idPT;
    }

    public String getIdCT() {
        return idCT.getText();
    }

    public void setIdCT(JTextField idCT) {
        this.idCT = idCT;
    }

    public String getQuantityT() {
        return quantityT.getText();
    }

    public void setQuantityT(JTextField quantityT) {
        this.quantityT = quantityT;
    }
}
