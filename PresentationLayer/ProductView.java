package PresentationLayer;

import Model.Client;
import Model.Product;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ProductView extends JFrame {

    JTable table = new JTable();
    private JLabel idP= new JLabel("ID PRODUCT:");
    private JLabel nameP= new JLabel("NAME PRODUCT:");
    private JLabel quantity= new JLabel("QUANTITY IN STOCK:");
    private JLabel price = new JLabel("PRICE :");
    private JTextField idPT=new JTextField(5);
    private JTextField namePT= new JTextField(5);
    private JTextField quantityT= new JTextField(5);
    private JTextField priceT= new JTextField(5);

    private JButton addProduct=new JButton("ADD PRODUCT");
    private JButton editProduct=new JButton("EDIT PRODUCT");
    private JButton deleteProduct=new JButton("DELETE PRODUCT");
    private JButton viewProducts=new JButton("VIEW PRODUCTS");
    private JPanel p1=new JPanel();
    private JPanel p2=new JPanel();
    private JScrollPane t2=new JScrollPane();
    public ProductView() {
        p1.add(idP);
        p1.add(idPT);
        p1.add(nameP);
        p1.add(namePT);
        p1.add(quantity);
        p1.add(quantityT);
        p1.add(price);
        p1.add(priceT);

        // p1.setAlignmentX(BOTTOM_ALIGNMENT);
        p1.add(t2);

        p2.add(addProduct);
        p2.add(editProduct);
        p2.add(deleteProduct);
        p2.add(viewProducts);

        p1.add(p2);

        this.add(p1);
        this.setTitle("Table Products");
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE)  ;
        //setBounds(160, 500, 600, 600);
        this.setBounds(500,500,700,600);
        //this.pack();
        //this.setVisible(true);
    }
    void addNewProductListener(ActionListener p1) {
        addProduct.addActionListener(p1);
    }

    void addEditProductListener(ActionListener p2) {
        editProduct.addActionListener(p2);
    }

    void addDeleteProductListener(ActionListener p3) {
        deleteProduct.addActionListener(p3);
    }

    void addViewProductsListener(ActionListener p4) {
        viewProducts.addActionListener(p4);
    }

    public JTable getTable() {
        return table;
    }

    public void setTable(JTable table) {
        this.table = table;
        t2.setViewportView(table);
    }

    public String getIdPT() {
        return idPT.getText();
    }

    public void setIdPT(JTextField idPT) {
        this.idPT = idPT;
    }

    public String getNamePT() {
        return namePT.getText();
    }

    public void setNamePT(JTextField namePT) {
        this.namePT = namePT;
    }

    public String getQuantityT() {
        return quantityT.getText();
    }

    public void setQuantityT(JTextField quantityT) {
        this.quantityT = quantityT;
    }

    public String getPriceT() {
        return priceT.getText();
    }

    public void setPriceT(JTextField priceT) {
        this.priceT = priceT;
    }
}
