package PresentationLayer;

import javax.swing.*;
import java.awt.event.ActionListener;

public class View {

    private JFrame f = new JFrame("");
    private JPanel panel1=new JPanel();
    private JButton produseButton=new JButton("Products");
    private JButton clientiButton=new JButton("Clients");
    private JButton createOrder=new JButton("Create Order");

    public View(){
        //f.setContentPane(panel1);
        panel1.add(clientiButton);
        panel1.add(produseButton);
        panel1.add(createOrder);
        f.add(panel1);
        f.setSize(400,200);

        f.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        // f.pack();
        f.setVisible(true);
    }
    void ClientListener(ActionListener p1) {
        clientiButton.addActionListener(p1);
    }

    void ProductListener(ActionListener p2) {
        produseButton.addActionListener(p2);
    }

    void CreateOrderListener(ActionListener e){createOrder.addActionListener(e);}
}
