package DataAcces;

import Model.Client;

public class ClientDAO extends AbstractDAO<Client>{

    public Client findClientById(int id){
        return super.findById(id);
    }
}
