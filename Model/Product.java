package Model;
/**
 * Clasa Product reprezinta produsul si datele utile desprea acesta
 * @author      Ciumafai Georgiana
 */
public class Product {

    private int idP;
    private String nameProduct;
    private int quantityInStock;
    private int price;
    /**
     * constructor cu 0 parametrii
     */
    public Product() {
    }
    /**
     * constructor
     * @param idP id produs
     * @param nameProduct nume produs
     * @param quantityInStock cantitate in stoc
     * @param price pret produs
     */
    public Product(int idP, String nameProduct, int quantityInStock, int price) {
        this.idP = idP;
        this.nameProduct = nameProduct;
        this.quantityInStock = quantityInStock;
        this.price = price;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public int getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(int quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "idP=" + idP +
                ", nameProduct=" + nameProduct +
                ", quantityInStock=" + quantityInStock +
                ", price=" + price +
                '}';
    }
}
