package DataAcces;

import Model.OrderClient;

public class OrderDAO extends AbstractDAO<OrderClient> {

    public OrderClient findOrderById(int id){
        return super.findById(id);
    }
}
