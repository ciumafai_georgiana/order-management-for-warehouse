package Model;
/**
 * Clasa Client reprezinta clientul si datele utile desprea acesta
 * @author      Ciumafai Georgiana
 */
public class Client {
    /**
     * id client
     */
    private int idC;
    /**
     * nume client
     */
    private String name;
    /**
     * adresa client
     */
    private String address;
    /**
     * telefon client
     */
    private String telephone;
    /**
     * constructor cu 0 parametrii
     */
    public Client() {
    }
    /**
     * constructor
     * @param idC id client
     * @param name nume client
     * @param address adresa client
     * @param telephone telefon client
     */
    public Client(int idC, String name, String address, String telephone) {
        this.idC = idC;
        this.name = name;
        this.address = address;
        this.telephone = telephone;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String toString() {
        return "Client{" +
                "idC=" + idC +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", telephone='" + telephone + '\'' +
                '}';
    }
}
