package Model;
/**
 * Clasa OrderClient reprezinta comanda si datele utile desprea aceasta
 * @author      Ciumafai Georgiana
 */
public class OrderClient {
    /**
     * id comanda
     */
    private int idO;
    /**
     * id client
     */
    private int idC;
    /**
     * id produs
     */
    private int idP;
    /**
     * cantitate comanda
     */
    private int quantity;
    /**
     * pret comanda
     */
    private int price;

    /**
     * constructor cu 0 parametrii
     */
    public OrderClient() {
    }
    /**
     * constructor
     * @param idO id-ul comenzii
     * @param idC id-ul clientului
     * @param idP id-ul produsului
     * @param quantity cantitatea de produs comandata de client
     * @param price pretul pentru toata comanda
     */
    public OrderClient(int idO, int idC, int idP, int quantity, int price) {
        this.idO = idO;
        this.idC = idC;
        this.idP = idP;
        this.quantity = quantity;
        this.price = price;
    }

    public int getIdO() {
        return idO;
    }

    public void setIdO(int idO) {
        this.idO = idO;
    }

    public int getIdC() {
        return idC;
    }

    public void setIdC(int idC) {
        this.idC = idC;
    }

    public int getIdP() {
        return idP;
    }

    public void setIdP(int idP) {
        this.idP = idP;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderClient{" +
                "idO=" + idO +
                ", idC=" + idC +
                ", idP=" + idP +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
