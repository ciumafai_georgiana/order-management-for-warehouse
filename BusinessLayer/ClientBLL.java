package BusinessLayer;

import DataAcces.ClientDAO;
import Model.Client;

import javax.swing.*;
import java.util.List;

public class ClientBLL {

    private static ClientDAO clientDAO;
    private List<Client> clienti;

    public ClientBLL() {
        clientDAO=new ClientDAO();

    }

    public List<Client> findAll() {
        List<Client> clients=clientDAO.findAll();
        return clients;
    }

    public void update(Client entity) {

        clientDAO.update(entity);
    }

    public Client findById(int id) {
        return clientDAO.findClientById(id);
    }

    public void delete(int id2) {
        clientDAO.deleteById(id2);
    }
    public void persist(Client entity) {

        clientDAO.insert(entity);
    }
    public  void deleteAll(){
        clientDAO.deleteAll();
    }
    public JTable getTable(){
        clienti=clientDAO.findAll();
        return clientDAO.createTable(clienti);
    }

}
