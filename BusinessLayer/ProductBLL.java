package BusinessLayer;

import DataAcces.ProductDAO;
import Model.Product;

import javax.swing.*;
import java.util.List;

public class ProductBLL {
    private static ProductDAO productDAO;
    private static List<Product> products;
    public ProductBLL() {
        productDAO=new ProductDAO();
    }
    public List<Product> findAll() {
        List<Product> productList=productDAO.findAll();
        return productList;
    }
    public void persist(Product entity) {

        productDAO.insert(entity);
    }
    public Product findById(int id) {
        return productDAO.findProductById(id);
    }

    public void update(Product entity) {

        productDAO.update(entity);
    }

    public void delete(int id2) {
        productDAO.deleteById(id2);
    }

    public void deleteAll(){
        productDAO.deleteAll();
    }

    public JTable getTable() {
        products=productDAO.findAll();
        return productDAO.createTable(products);
    }
}
