package BusinessLayer;

import DataAcces.OrderDAO;
import Model.OrderClient;


import javax.swing.*;
import java.util.List;

public class OrderBLL  {
    private static OrderDAO orderDAO;
    private static List<OrderClient> orderClient;

    public OrderBLL() {
        orderDAO=new OrderDAO();

    }

    public List<OrderClient> findAll() {
        List<OrderClient> OrderList=orderDAO.findAll();
        return OrderList;
    }
    public void persist(OrderClient entity) {

        orderDAO.insert(entity);
    }
    public OrderClient findById(int id) {
        return orderDAO.findOrderById(id);
    }

    public void update(OrderClient entity) {

        orderDAO.update(entity);
    }

    public void delete(int id2) {
        orderDAO.deleteById(id2);
    }

    public void deleteAll(){
        orderDAO.deleteAll();
    }

    public JTable getTable() {
        orderClient=orderDAO.findAll();
        return orderDAO.createTable(orderClient);
    }
}
